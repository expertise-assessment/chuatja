# Chuatja

### Find out the **weather** information of your city.<br/><br/>
![chuatja_demo](https://gitlab.com/expertise-assessment/chuatja/-/wikis/uploads/55b1b2d1e4939dd1cfcf31fe9b872a37/chuatja_demo.mp4)
<br/>

## App Architecture

This app uses **Clean Architecture** which adopts [Uncle Bob's](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html) and [Official Android's](https://developer.android.com/topic/architecture) one.<br/>
![image info](https://gitlab.com/expertise-assessment/chuatja/-/wikis/uploads/0c88a751005add475c43817904903c7f/Clean-Architecture-Flutter-Diagram.webp)
<br/>

## Package dependencies
  - Default Flutter SDK
  - Functional programming thingies<br/>`dartz: ^0.10.1`
  - Value equality<br/>`equatable: ^2.0.5`
  - Theming<br/>`flex_color_scheme: ^7.3.1`
  - Bloc for state management<br/>`flutter_bloc: ^8.1.3`
  - Svg pictures handler<br/>`flutter_svg: ^2.0.9`
  - Service locator<br/>`get_it: ^7.6.4`
  - Remote API client<br/>`http: ^1.1.1`
  - Mock framework<br/>`mockito: ^5.4.3`
  - Environment variables secure handler<br/>`envied: ^0.5.2`
  - Dependency injection<br/>`injectable: ^2.3.2`<br/>`injectable_generator: ^2.4.1`
<br/>

## How to run project in your local machine

  1. Create `.env.dev` file containing this value
      ```
      API_BASE_URL = 'https://api.weatherapi.com/v1/'
      API_KEY = '<your_own_api_key>'
      API_CDN_URL = 'https://cdn.weatherapi.com/'
      ```
      Please use `<your_own_api_key>` generated from your [weatherapi.com dashboard](https://www.weatherapi.com/my/).

  2. [Install](https://docs.flutter.dev/get-started/install) Flutter SDK
  3.  Run command to setup everything <i>(get package dependencies and generate config)</i>
      ```
      flutter clean && flutter pub get && flutter pub run build_runner build --delete-conflicting-outputs && flutter test --coverage
      ```
  4.  Run the project
      ```
      flutter run
      ```
<br/>

## Next possible development
  - [ ] Internet connection checker
  - [ ] Location advanced search feature
  - [ ] Firebase integration
  - [ ] Flexible theme config

<br/>