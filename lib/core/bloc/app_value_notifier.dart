import 'package:flutter/material.dart';

class AppValueNotifier {
  static final ValueNotifier<ThemeMode> themeNotifier =
      ValueNotifier(ThemeMode.system);

  static const dayThemeMode = ThemeMode.light;
  static const nightThemeMode = ThemeMode.dark;
}
