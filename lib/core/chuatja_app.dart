import 'package:chuatja/core/bloc/app_value_notifier.dart';
import 'package:chuatja/core/presentation/app_themes.dart';
import 'package:chuatja/features/weather/presentation/screens/home/home_screen.dart';
import 'package:flutter/material.dart';

class ChuatjaApp extends StatelessWidget {
  const ChuatjaApp({super.key});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus &&
            currentFocus.focusedChild != null) {
          FocusManager.instance.primaryFocus?.unfocus();
        }
      },
      child: ValueListenableBuilder<ThemeMode>(
        valueListenable: AppValueNotifier.themeNotifier,
        builder: (_, ThemeMode currentMode, __) {
          return MaterialApp(
            debugShowCheckedModeBanner: false,
            theme: AppThemes.chuatjaDay,
            darkTheme: AppThemes.chuatjaNight,
            themeMode: currentMode,
            home: const HomeScreen(),
          );
        },
      ),
    );
  }
}
