import 'package:equatable/equatable.dart';

abstract class Failure extends Equatable {
  @override
  List<Object> get props => [];
}

class ServerFailure extends Failure {}

class LocationQueryFailure extends Failure {}

const String defaultFailureMessage = 'Unexpected Error.';
const String locationFailureMessage = 'No matching location found.';
