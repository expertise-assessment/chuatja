import 'package:chuatja/features/weather/domain/entities/location.dart';

class LocationModel extends Location {
  const LocationModel({
    required super.locationName,
    required super.locationLatitude,
    required super.locationLongitude,
    required super.localtime,
  });

  factory LocationModel.fromJson(Map<String, dynamic> json) {
    return LocationModel(
      locationName: json['location'] != null ? json['location']['name'] : '',
      locationLatitude:
          json['location'] != null ? json['location']['lat'] : 0.0,
      locationLongitude:
          json['location'] != null ? json['location']['lon'] : 0.0,
      localtime: json['location'] != null ? json['location']['localtime'] : 0,
    );
  }
}
