import 'package:chuatja/features/weather/domain/entities/weather_forecast_hour.dart';

class WeatherForecastHourModel extends WeatherForecastHour {
  const WeatherForecastHourModel({
    required super.time,
    required super.temperatureCelsius,
    required super.conditionIcon,
    required super.windDirection,
    required super.humidity,
    required super.uvIndex,
  });

  factory WeatherForecastHourModel.fromJson(Map<String, dynamic> json,
      [int dayIndex = 0, int hourIndex = 0]) {
    return WeatherForecastHourModel(
      time: json['forecast'] != null
          ? json['forecast']['forecastday'][dayIndex]['hour'] != null
              ? json['forecast']['forecastday'][dayIndex]['hour'][hourIndex]
                  ['time']
              : ''
          : '',
      temperatureCelsius: json['forecast'] != null
          ? json['forecast']['forecastday'][dayIndex]['hour'] != null
              ? json['forecast']['forecastday'][dayIndex]['hour'][hourIndex]
                  ['temp_c']
              : 0.0
          : 0.0,
      conditionIcon: json['forecast'] != null
          ? json['forecast']['forecastday'][dayIndex]['hour'] != null
              ? json['forecast']['forecastday'][dayIndex]['hour'][hourIndex]
                  ['condition']['icon']
              : ''
          : '',
      windDirection: json['forecast'] != null
          ? json['forecast']['forecastday'][dayIndex]['hour'] != null
              ? json['forecast']['forecastday'][dayIndex]['hour'][hourIndex]
                  ['wind_dir']
              : ''
          : '',
      humidity: json['forecast'] != null
          ? json['forecast']['forecastday'][dayIndex]['hour'] != null
              ? json['forecast']['forecastday'][dayIndex]['hour'][hourIndex]
                  ['humidity']
              : 0
          : 0,
      uvIndex: json['forecast'] != null
          ? json['forecast']['forecastday'][dayIndex]['hour'] != null
              ? json['forecast']['forecastday'][dayIndex]['hour'][hourIndex]
                  ['uv']
              : 0.0
          : 0.0,
    );
  }
}
