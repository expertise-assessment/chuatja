import 'dart:convert';

import 'package:chuatja/core/error/exception.dart';
import 'package:chuatja/env/env.dart';
import 'package:chuatja/features/weather/data/models/location_model.dart';
import 'package:chuatja/features/weather/data/models/weather_current_model.dart';
import 'package:chuatja/features/weather/data/models/weather_forecast_astro_model.dart';
import 'package:chuatja/features/weather/data/models/weather_forecast_hour_model.dart';
import 'package:chuatja/features/weather/data/models/weather_forecast_model.dart';
import 'package:chuatja/features/weather/data/models/weather_model.dart';
import 'package:chuatja/features/weather/domain/usecases/get_weather_usecase.dart';
import 'package:http/http.dart' as http;
import 'package:injectable/injectable.dart';

abstract class WeatherDataSource {
  /// Calls the get forecast weather from the [API] endpoints,
  ///
  /// Returns location, current and forecast weather info.
  /// Throws a [ServerException] for all error codes.
  Future<WeatherModel?>? getWeather(WeatherParams params);
}

final apiBaseUrl = Env.apiBaseUrl;
final apiKey = Env.apiKey;
final apiCDNUrl = Env.apiCDNUrl;

@LazySingleton(as: WeatherDataSource)
class WeatherDataSourceImpl implements WeatherDataSource {
  final http.Client client;

  WeatherDataSourceImpl({required this.client});

  static final endpoint = {
    'getWeatherForecast': 'forecast.json',
  };

  String getQueryString(String location) {
    return '?key=$apiKey&q=$location&days=2';
  }

  Future<http.Response> _fetchApi(String url) async {
    final uri = Uri.parse(url);
    final response = await client.get(uri);
    if (response.statusCode != 200) {
      final Map<String, dynamic> jsonContent = await json.decode(response.body);
      final int errorCode = jsonContent['error']['code'];    
      switch (errorCode) {
        case 1006:
          throw LocationQueryException();
        default:
          throw ServerException();
      }
    }
    return response;
  }

  List<WeatherForecastHourModel> transformForecastHoursModel(
      Map<String, dynamic> jsonContent) {
    final List<WeatherForecastHourModel> weatherForecastHourModelList = [];
    for (int day = 0; day < 2; day++) {
      for (int hour = 0; hour < 24; hour++) {
        if (day == 1 && hour == 1) break;
        final hourModel = WeatherForecastHourModel.fromJson(
          jsonContent,
          day,
          hour,
        );
        weatherForecastHourModelList.add(hourModel);
      }
    }
    return weatherForecastHourModelList;
  }

  WeatherModel transformJsonToModel(
      Map<String, dynamic> jsonContent) {
    WeatherModel weatherModel;
    final locationModel = LocationModel.fromJson(jsonContent);
    final weatherCurrentModel = WeatherCurrentModel.fromJson(jsonContent);
    final weatherForecastAstroModel =
        WeatherForecastAstroModel.fromJson(jsonContent);
    final List<WeatherForecastHourModel> weatherForecastHourModelList =
        transformForecastHoursModel(jsonContent);
    final weatherForecastModel = WeatherForecastModel(
      astro: weatherForecastAstroModel,
      hours: weatherForecastHourModelList,
    );
    weatherModel = WeatherModel(
      location: locationModel,
      weatherCurrent: weatherCurrentModel,
      weatherForecast: weatherForecastModel,
    );
    return weatherModel;
  }

  Future<WeatherModel> fetchWeatherForecast(String url) async {
    WeatherModel? weatherModel;
    final http.Response response = await _fetchApi(url);
    final Map<String, dynamic> jsonContent = json.decode(response.body);
    weatherModel = transformJsonToModel(jsonContent);

    return Future.value(weatherModel);
  }

  @override
  Future<WeatherModel?>? getWeather(WeatherParams params) async {
    final queryString = getQueryString(params.searchKeyword);
    final endpointUrl =
        apiBaseUrl + endpoint['getWeatherForecast']! + queryString;

    final weatherData = await fetchWeatherForecast(endpointUrl);
    return Future.value(weatherData);
  }
}
