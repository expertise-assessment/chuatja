import 'package:chuatja/features/weather/domain/entities/weather_forecast_astro.dart';
import 'package:chuatja/features/weather/domain/entities/weather_forecast_hour.dart';
import 'package:equatable/equatable.dart';

class WeatherForecast extends Equatable {
  final WeatherForecastAstro astro;
  final List<WeatherForecastHour> hours;

  const WeatherForecast({
    required this.astro,
    required this.hours,
  });

  @override
  List<Object?> get props => [astro, hours];
}
