import 'package:equatable/equatable.dart';

class WeatherForecastHour extends Equatable {
  final String time;
  final double temperatureCelsius;
  final String conditionIcon;
  final String windDirection;
  final int humidity;
  final double uvIndex;

  const WeatherForecastHour({
    required this.time,
    required this.temperatureCelsius,
    required this.conditionIcon,
    required this.windDirection,
    required this.humidity,
    required this.uvIndex,
  });

  @override
  List<Object?> get props => [
        time,
        temperatureCelsius,
        conditionIcon,
        windDirection,
        humidity,
        uvIndex,
      ];
}
