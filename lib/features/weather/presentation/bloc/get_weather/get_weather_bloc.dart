import 'package:chuatja/core/bloc/app_value_notifier.dart';
import 'package:chuatja/core/error/failure.dart';
import 'package:chuatja/features/weather/domain/entities/weather.dart';
import 'package:chuatja/features/weather/domain/usecases/get_weather_usecase.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';

part 'get_weather_event.dart';
part 'get_weather_state.dart';

@injectable
class GetWeatherBloc extends Bloc<GetWeatherEvent, GetWeatherState> {
  final GetWeatherUsecase getWeatherUsecase;
  GetWeatherBloc({required this.getWeatherUsecase})
      : super(GetWeatherInitializing()) {
    on<GetWeatherInit>((event, emit) async => emit(GetWeatherLoading()));

    on<GetWeather>((event, emit) async {
      WeatherParams params = WeatherParams();

      if (event.searchKeyword != '') params.searchKeyword = event.searchKeyword;
      final failureOrWeather = await getWeatherUsecase(params);
      emit(
        failureOrWeather!.fold(
          (failure) => GetWeatherError(
            message: _mapFailureToMessage(failure),
          ),
          (weatherData) {
            AppValueNotifier.themeNotifier.value =
                (weatherData?.weatherCurrent.isDay == 0)
                    ? AppValueNotifier.nightThemeMode
                    : AppValueNotifier.dayThemeMode;
            return GetWeatherLoaded(weather: weatherData!);
          },
        ),
      );
    });
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case const (LocationQueryFailure):
        return locationFailureMessage;
      default:
        return defaultFailureMessage;
    }
  }
}
