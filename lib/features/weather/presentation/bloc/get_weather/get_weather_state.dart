part of 'get_weather_bloc.dart';

abstract class GetWeatherState extends Equatable {
  const GetWeatherState();

  @override
  List<Object> get props => [];
}

class GetWeatherInitializing extends GetWeatherState {}

class GetWeatherLoading extends GetWeatherState {}

class GetWeatherLoaded extends GetWeatherState {
  final Weather weather;

  const GetWeatherLoaded({required this.weather});

  @override
  List<Object> get props => [weather];
}

class GetWeatherError extends GetWeatherState {
  final String message;

  const GetWeatherError({required this.message});

  @override
  List<Object> get props => [message];
}
