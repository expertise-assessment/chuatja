import 'package:flutter/material.dart';

class AstroDetailWidget extends StatelessWidget {
  const AstroDetailWidget({
    super.key,
    required this.astroField,
    required this.astroTime,
  });

  final String astroField;
  final String astroTime;

  @override
  Widget build(BuildContext context) {
    final String field = astroField;
    final String time = astroTime;
    return SizedBox(
      child: Column(
        children: [
          Expanded(
            child: Container(
              alignment: Alignment.bottomCenter,
              child: Text(field),
            ),
          ),
          Expanded(
            child: Container(
              alignment: Alignment.topCenter,
              child: Text(time),
            ),
          )
        ],
      ),
    );
  }
}
