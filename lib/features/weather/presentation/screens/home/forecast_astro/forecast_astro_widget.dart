import 'package:chuatja/features/weather/domain/entities/weather_forecast_astro.dart';
import 'package:chuatja/features/weather/presentation/screens/home/forecast_astro/astro_detail_widget.dart';
import 'package:flutter/material.dart';

class ForecastAstroWidget extends StatelessWidget {
  const ForecastAstroWidget({super.key, required this.astro});

  final WeatherForecastAstro astro;

  @override
  Widget build(BuildContext context) {
    final String astroSunriseTime = astro.astroSunriseTime;
    final String astroSunsetTime = astro.astroSunsetTime;
    final String astroMoonriseTime = astro.astroMoonriseTime;
    final String astroMoonsetTime = astro.astroMoonsetTime;
    return SizedBox(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          AstroDetailWidget(
            astroField: 'Sunrise',
            astroTime: astroSunriseTime,
          ),
          AstroDetailWidget(
            astroField: 'Sunset',
            astroTime: astroSunsetTime,
          ),
          AstroDetailWidget(
            astroField: 'Moonrise',
            astroTime: astroMoonriseTime,
          ),
          AstroDetailWidget(
            astroField: 'Moonset',
            astroTime: astroMoonsetTime,
          ),
        ],
      ),
    );
  }
}
