import 'package:chuatja/features/weather/domain/entities/weather_forecast_hour.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ForecastHourButtonWidget extends StatelessWidget {
  const ForecastHourButtonWidget({
    super.key,
    required this.hour,
    required this.index,
    required this.isSelected,
    required this.onSelect,
  });

  final WeatherForecastHour hour;
  final int index;
  final bool isSelected;
  final VoidCallback onSelect;

  @override
  Widget build(BuildContext context) {
    final int temperatureCelcius = hour.temperatureCelsius.floor();
    final String temperatureInfo = '$temperatureCelcius\u00B0';

    final String conditionIcon = 'https:${hour.conditionIcon}';

    final DateTime time = DateTime.parse(hour.time);
    final String timeString = DateFormat.j().format(time);

    return FilledButton(
      onPressed: onSelect,
      style: ButtonStyle(
        backgroundColor: MaterialStatePropertyAll(
          isSelected
              ? Theme.of(context).colorScheme.primary
              : Theme.of(context).colorScheme.primaryContainer.withOpacity(0.2),
        ),
      ),
      child: Column(
        children: [
          Expanded(
            child: SizedBox(
              width: 60.0,
              child: Column(
                children: [
                  Expanded(
                    child: Container(
                      alignment: Alignment.center,
                      child: Text(
                        temperatureInfo,
                        style: TextStyle(
                          color: isSelected
                              ? Theme.of(context).cardColor
                              : Theme.of(context).primaryColor,
                          fontSize: 20.0,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Image.network(
                      conditionIcon,
                    ),
                  ),
                  Expanded(
                    child: Container(
                      alignment: Alignment.center,
                      child: Text(
                        timeString,
                        style: TextStyle(
                          color: isSelected
                              ? Theme.of(context).cardColor
                              : Theme.of(context).primaryColor,
                          fontSize: 15.0,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
