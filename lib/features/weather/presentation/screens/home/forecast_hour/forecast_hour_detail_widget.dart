import 'package:chuatja/features/weather/domain/entities/weather_forecast_hour.dart';
import 'package:flutter/material.dart';

class ForecastHourDetailWidget extends StatelessWidget {
  const ForecastHourDetailWidget({super.key, required this.hour});

  final WeatherForecastHour hour;

  @override
  Widget build(BuildContext context) {
    final int temperatureCelcius = hour.temperatureCelsius.floor();
    final String temperatureInfo = '$temperatureCelcius\u00B0';

    final String windDirection = hour.windDirection;

    final String humidity = hour.humidity.toString();

    final String uvIndex = hour.uvIndex.toString();

    return SizedBox(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          SizedBox(
            child: Column(
              children: [
                Expanded(
                  child: Container(
                    alignment: Alignment.center,
                    child: Text(
                      'Temperature\n(C)',
                      style: TextStyle(
                        color: Theme.of(context).cardColor,
                        fontWeight: FontWeight.w500,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    alignment: Alignment.center,
                    child: Text(
                      temperatureInfo,
                      style: TextStyle(
                        color: Theme.of(context).cardColor,
                        fontWeight: FontWeight.w700,
                        fontSize: 32.0,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            child: Column(
              children: [
                Expanded(
                  child: Container(
                    alignment: Alignment.center,
                    child: Text(
                      'Wind\nDirection',
                      style: TextStyle(
                        color: Theme.of(context).cardColor,
                        fontWeight: FontWeight.w500,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    alignment: Alignment.center,
                    child: Text(
                      windDirection,
                      style: TextStyle(
                        color: Theme.of(context).cardColor,
                        fontWeight: FontWeight.w700,
                        fontSize: 32.0,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
          SizedBox(
            child: Column(
              children: [
                Expanded(
                  child: Container(
                    alignment: Alignment.center,
                    child: Text(
                      'Humidity\n(%)',
                      style: TextStyle(
                        color: Theme.of(context).cardColor,
                        fontWeight: FontWeight.w500,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    alignment: Alignment.center,
                    child: Text(
                      humidity,
                      style: TextStyle(
                        color: Theme.of(context).cardColor,
                        fontWeight: FontWeight.w700,
                        fontSize: 32.0,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            child: Column(
              children: [
                Expanded(
                  child: Container(
                    alignment: Alignment.center,
                    child: Text(
                      'UV\nIndex',
                      style: TextStyle(
                        color: Theme.of(context).cardColor,
                        fontWeight: FontWeight.w500,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    alignment: Alignment.center,
                    child: Text(
                      uvIndex,
                      style: TextStyle(
                        color: Theme.of(context).cardColor,
                        fontWeight: FontWeight.w700,
                        fontSize: 32.0,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
