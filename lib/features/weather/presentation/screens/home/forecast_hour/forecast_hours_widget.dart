import 'package:chuatja/features/weather/domain/entities/weather_forecast_hour.dart';
import 'package:chuatja/features/weather/presentation/screens/home/forecast_hour/forecast_hour_button_widget.dart';
import 'package:chuatja/features/weather/presentation/screens/home/forecast_hour/forecast_hour_detail_widget.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';

class ForecastHoursWidget extends StatefulWidget {
  const ForecastHoursWidget({
    super.key,
    required this.hours,
    required this.currentTimeString,
  });
  final List<WeatherForecastHour> hours;
  final String currentTimeString;

  @override
  State<ForecastHoursWidget> createState() => _ForecastHoursWidgetState();
}

class _ForecastHoursWidgetState extends State<ForecastHoursWidget> {
  late int currentSelectedIndex;
  WeatherForecastHour? chosenForecastHour;

  final itemController = ItemScrollController();

  Future scrollToCurrentTime() async {
    setState(() {
      currentSelectedIndex = currentTime;
    });
    itemController.scrollTo(
      index: currentTime,
      duration: const Duration(milliseconds: 500),
      curve: Curves.easeIn,
    );
  }

  late int currentTime;
  @override
  void initState() {
    setupCurrentTime();
    super.initState();
  }

  void setupCurrentTime() {
    final DateTime time = DateTime.parse(widget.currentTimeString);
    final String timeInHourString = DateFormat.H().format(time);
    final String timeInMinuteString = DateFormat.m().format(time);

    final int convertedTimeInHour = (int.parse(timeInMinuteString) < 31)
        ? int.parse(timeInHourString)
        : (int.parse(timeInHourString) + 1);
    currentTime = convertedTimeInHour;
    currentSelectedIndex = currentTime;
  }

  @override
  void didUpdateWidget(covariant ForecastHoursWidget oldWidget) {
    setupCurrentTime();
    super.didUpdateWidget(oldWidget);
    WidgetsBinding.instance.addPostFrameCallback((_) {
      scrollToCurrentTime();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: Container(
            alignment: Alignment.bottomLeft,
            child: GestureDetector(
              onTap: scrollToCurrentTime,
              child: const Text(
                'Today Forecast',
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 16.0,
                  letterSpacing: 0.38,
                ),
              ),
            ),
          ),
        ),
        Expanded(
          flex: 5,
          child: Container(
            margin: const EdgeInsets.only(top: 20.0),
            padding: const EdgeInsets.all(16.0),
            decoration: BoxDecoration(
              color: Theme.of(context)
                  .colorScheme
                  .primaryContainer
                  .withOpacity(0.25),
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.circular(20.0),
            ),
            child: ScrollablePositionedList.separated(
              itemScrollController: itemController,
              scrollDirection: Axis.horizontal,
              separatorBuilder: (context, index) => const SizedBox(width: 8.0),
              itemCount: currentTime >= 23
                  ? widget.hours.length
                  : widget.hours.length - 1,
              itemBuilder: (BuildContext context, int index) {
                bool isSameSelection = currentSelectedIndex == index;
                return ForecastHourButtonWidget(
                  hour: widget.hours[index],
                  index: index,
                  isSelected: isSameSelection,
                  onSelect: () {
                    setState(() {
                      currentSelectedIndex = isSameSelection ? index : index;
                    });
                  },
                );
              },
            ),
          ),
        ),
        Expanded(
          flex: 4,
          child: Container(
            margin: const EdgeInsets.only(top: 20.0),
            padding: const EdgeInsets.all(16.0),
            decoration: BoxDecoration(
              color: Theme.of(context).colorScheme.primary,
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.circular(20.0),
            ),
            child: ForecastHourDetailWidget(
              hour: widget.hours[currentSelectedIndex],
            ),
          ),
        ),
      ],
    );
  }
}
