import 'package:chuatja/core/injection_container.dart';
import 'package:chuatja/core/presentation/background_image.dart';
import 'package:chuatja/features/weather/presentation/bloc/get_weather/get_weather_bloc.dart';
import 'package:chuatja/features/weather/presentation/screens/home/weather_current_widget.dart';
import 'package:chuatja/features/weather/presentation/screens/home/weather_forecast_widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {
  late final GetWeatherBloc weatherBloc;
  bool isLoading = false;
  late final String backgroundImage;

  final TextEditingController _searchController = TextEditingController();
  late String inputStr;
  final FocusNode _searchFocus = FocusNode();

  void _getWeatherBlocListener(state) {
    if (state is GetWeatherError) {
      setState(() {
        isLoading = false;
      });
    } else if (state is GetWeatherLoaded) {
      setState(() {
        isLoading = false;
      });
    } else {
      setState(() {
        isLoading = true;
      });
    }
  }

  @override
  void initState() {
    weatherBloc = getIt<GetWeatherBloc>();
    weatherBloc.stream.listen(_getWeatherBlocListener);
    weatherBloc.add(GetWeatherInit());
    backgroundImage = 'assets/images/city-background.svg';
    inputStr = '';

    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      weatherBloc.add(const GetWeather());
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Theme.of(context).colorScheme.background,
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              _buildSearchInput(),
              const Divider(height: 3),
              _buildContent(context),
            ],
          ),
        ),
      ),
    );
  }

  BlocProvider<GetWeatherBloc> _buildContent(BuildContext context) {
    return BlocProvider(
      create: (context) => weatherBloc,
      child: Expanded(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: BlocBuilder<GetWeatherBloc, GetWeatherState>(
            builder: (context, state) {
              if (state is GetWeatherLoaded) {
                return Stack(
                  children: [
                    Center(
                      child: BackgroundImage(
                        assetName: backgroundImage,
                        color: Theme.of(context).colorScheme.primary,
                        height: MediaQuery.of(context).size.height / 5,
                        width: MediaQuery.of(context).size.width / 5,
                        fit: BoxFit.cover,
                      ),
                    ),
                    Column(
                      children: [
                        Expanded(
                          flex: 2,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                              horizontal: 16.0,
                            ),
                            child: WeatherCurrentWidget(
                              weatherCurrentData: state.weather,
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 5,
                          child: WeatherForecastWidgets(
                            weatherForecastData: state.weather,
                          ),
                        ),
                      ],
                    ),
                  ],
                );
              } else if (state is GetWeatherError) {
                return Column(
                  children: [
                    Text(
                      state.message,
                      style: const TextStyle(fontSize: 24),
                      textAlign: TextAlign.center,
                    ),
                  ],
                );
              }
              return const Center(child: CircularProgressIndicator());
            },
          ),
        ),
      ),
    );
  }

  void _dispatchGetWeather() {
    setState(() {
      isLoading = true;
    });
    if (inputStr.isNotEmpty) {
      weatherBloc.add(GetWeather(searchKeyword: inputStr));
    } else {
      weatherBloc.add(const GetWeather());
    }
  }

  Widget _buildSearchInput() {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: TextFormField(
        controller: _searchController,
        textInputAction: TextInputAction.search,
        decoration: const InputDecoration(
          prefixIcon: Icon(Icons.search),
          labelText: 'Which location are you looking for?',
          contentPadding: EdgeInsets.only(left: 20.0),
        ),
        focusNode: _searchFocus,
        onChanged: (value) {
          setState(() {
            inputStr = value;
          });
        },
        onFieldSubmitted: (_) {
          _dispatchGetWeather();
        },
      ),
    );
  }
}
