import 'package:chuatja/features/weather/domain/entities/weather.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class WeatherCurrentWidget extends StatelessWidget {
  const WeatherCurrentWidget({
    super.key,
    required this.weatherCurrentData,
  });

  final Weather weatherCurrentData;

  @override
  Widget build(BuildContext context) {
    final String lastUpdatedTime =
        weatherCurrentData.weatherCurrent.lastUpdatedTime;
    final String localtimeInfo =
        DateFormat('d MMMM yyyy').format(DateTime.parse(lastUpdatedTime));

    final String locationName = weatherCurrentData.location.locationName;
    double tooltipPosition = locationName.length.toDouble() * 10;
    final String locationDetail = '''$locationName
    Lat: ${weatherCurrentData.location.locationLatitude}
    Lon: ${weatherCurrentData.location.locationLongitude}''';

    final int temperatureCelcius =
        weatherCurrentData.weatherCurrent.temperatureCelsius.floor();
    final String temperatureInfo = '$temperatureCelcius\u00B0';

    final String conditionText =
        weatherCurrentData.weatherCurrent.conditionText;
    final String conditionIcon =
        'https:${weatherCurrentData.weatherCurrent.conditionIcon}';

    return Row(
      children: [
        Expanded(
          flex: 3,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(localtimeInfo),
              const SizedBox(height: 8.0),
              SizedBox(
                height: 41.0,
                child: Tooltip(
                  message: locationDetail,
                  triggerMode: TooltipTriggerMode.tap,
                  showDuration: const Duration(seconds: 5),
                  verticalOffset: -13,
                  margin: EdgeInsets.only(left: tooltipPosition),
                  child: Text(
                    locationName,
                    style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 34.0,
                    ),
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ),
              Expanded(
                child: SizedBox(
                  height: 70.0,
                  child: Text(
                    temperatureInfo,
                    style: const TextStyle(
                      fontSize: 96.0,
                      letterSpacing: 0.37,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        Expanded(
          flex: 2,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Tooltip(
                message: conditionText,
                triggerMode: TooltipTriggerMode.tap,
                showDuration: const Duration(seconds: 5),
                verticalOffset: -13,
                margin: EdgeInsets.only(left: tooltipPosition),
                child: Container(
                  alignment: Alignment.centerRight,
                  child: Text(
                    conditionText,
                    style: const TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 20.0,
                      letterSpacing: 0.38,
                    ),
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ),
              const SizedBox(height: 8.0),
              Expanded(
                child: Container(
                  alignment: Alignment.topRight,
                  child: Container(
                    width: 100.0,
                    height: 100.0,
                    decoration: BoxDecoration(
                      color: Theme.of(context)
                          .colorScheme
                          .primaryContainer
                          .withOpacity(0.25),
                      shape: BoxShape.circle,
                    ),
                    child: Image.network(
                      conditionIcon,
                      scale: 0.8,
                    ),
                  ),
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
