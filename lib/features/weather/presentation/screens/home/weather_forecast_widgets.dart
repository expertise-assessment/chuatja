import 'package:chuatja/features/weather/domain/entities/weather.dart';
import 'package:chuatja/features/weather/presentation/screens/home/forecast_astro/forecast_astro_widget.dart';
import 'package:chuatja/features/weather/presentation/screens/home/forecast_hour/forecast_hours_widget.dart';
import 'package:flutter/material.dart';

class WeatherForecastWidgets extends StatelessWidget {
  const WeatherForecastWidgets({
    super.key,
    required this.weatherForecastData,
  });

  final Weather weatherForecastData;

  @override
  Widget build(BuildContext context) {
    final weatherForecastAstro = weatherForecastData.weatherForecast.astro;
    final weatherForecastHours = weatherForecastData.weatherForecast.hours;

    final currentLocalTime = weatherForecastData.location.localtime;
    final formattedHourTime =
        currentLocalTime.split(' ')[1].split(':')[0].padLeft(2, '0');
    final formattedLocalDateTime =
        '${currentLocalTime.split(' ')[0]} $formattedHourTime:${currentLocalTime.split(' ')[1].split(':')[1]}';

    return Column(
      children: [
        Expanded(
          child: ForecastAstroWidget(astro: weatherForecastAstro),
        ),
        Expanded(
          flex: 9,
          child: ForecastHoursWidget(
            hours: weatherForecastHours,
            currentTimeString: formattedLocalDateTime,
          ),
        ),
      ],
    );
  }
}
