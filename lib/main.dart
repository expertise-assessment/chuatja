import 'package:chuatja/core/chuatja_app.dart';
import 'package:chuatja/core/injection_container.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]);
  await configureDependencies();
  runApp(const ChuatjaApp());
}
