import 'package:chuatja/features/weather/domain/entities/weather_forecast.dart';
import 'package:chuatja/features/weather/domain/entities/weather_forecast_astro.dart';
import 'package:chuatja/features/weather/domain/entities/weather_forecast_hour.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('should return true when comparing identical object attributes', () {
    const weatherForecastAstro = WeatherForecastAstro(
      astroSunriseTime: "2023-11-22 00:01",
      astroSunsetTime: "2023-11-22 00:02",
      astroMoonriseTime: "2023-11-22 00:03",
      astroMoonsetTime: "2023-11-22 00:04",
    );
    const weatherForecastHour = WeatherForecastHour(
      time: "2023-11-23 00:00",
      temperatureCelsius: 20.0,
      conditionIcon: "ClearIcon",
      windDirection: "N",
      humidity: 1,
      uvIndex: 1.0,
    );
    const List<WeatherForecastHour> hours = [weatherForecastHour];
    const weatherForecast1 = WeatherForecast(
      astro: weatherForecastAstro,
      hours: hours,
    );
    const weatherForecast2 = WeatherForecast(
      astro: weatherForecastAstro,
      hours: hours,
    );

    expect(weatherForecast1.props, weatherForecast2.props);
  });
}
