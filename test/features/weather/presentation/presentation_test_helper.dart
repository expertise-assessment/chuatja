import 'package:chuatja/features/weather/domain/entities/location.dart';
import 'package:chuatja/features/weather/domain/entities/weather.dart';
import 'package:chuatja/features/weather/domain/entities/weather_current.dart';
import 'package:chuatja/features/weather/domain/entities/weather_forecast.dart';
import 'package:chuatja/features/weather/domain/entities/weather_forecast_astro.dart';
import 'package:chuatja/features/weather/domain/entities/weather_forecast_hour.dart';

const tLocation = Location(
  locationName: 'Jakarta',
  locationLatitude: -6.21,
  locationLongitude: 106.85,
  localtime: '2023-11-23 1:22',
);
const tWeatherCurrent = WeatherCurrent(
  lastUpdatedEpoch: 1700676900,
  lastUpdatedTime: "2023-11-23 01:45",
  temperatureCelsius: 27.0,
  isDay: 0,
  conditionText: "Partly cloudy",
  conditionIcon: "{apiCDNUrl}weather/64x64/night/116.png",
);
const tWeatherForecastAstro = WeatherForecastAstro(
  astroSunriseTime: "05:26 AM",
  astroSunsetTime: "05:52 PM",
  astroMoonriseTime: "02:18 PM",
  astroMoonsetTime: "01:53 AM",
);
const tWeatherForecastHour = WeatherForecastHour(
  time: "2023-11-23 00:00",
  temperatureCelsius: 29.2,
  conditionIcon: "{apiCDNUrl}weather/64x64/night/113.png",
  windDirection: "WSW",
  humidity: 69,
  uvIndex: 1.0,
);
const tWeatherForecast = WeatherForecast(
  astro: tWeatherForecastAstro,
  hours: [tWeatherForecastHour],
);
const tWeather = Weather(
  location: tLocation,
  weatherCurrent: tWeatherCurrent,
  weatherForecast: tWeatherForecast,
);
